import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {ClienteInterface} from '../interfaces/cliente-interface';

@Injectable()
export class ClientesRestService {
  constructor(
    private readonly _http: HttpClient
  ) {
  }

  findAll(skip: number, take: number, relations: number, busquedaLike?: string) {
    if (busquedaLike) {
      return this._http.get('http://localhost:3000/cliente?skip=' + skip + '&take=' + take + '&relations=' + relations + '&busquedaLike=' + busquedaLike);
    } else {
      return this._http.get('http://localhost:3000/cliente?skip=' + skip + '&take=' + take + '&relations=' + relations);
    }
  }

  findOne(idcliente: number) {
    return this._http.get('http://localhost:3000/cliente?id=' + idcliente);
  }

  createOne(cliente: ClienteInterface) {

    return this._http.post('http://localhost:3000/cliente', {
      cliente: {
        nombres: cliente.nombres,
        apellidos: cliente.apellidos
      }
    });
  }

  updateOne(idCliente: number, cliente: ClienteInterface) {
    return this._http.put('http://localhost:3000/cliente', {
      cliente: {
        id: idCliente,
        nombres: cliente.nombres,
        apellidos: cliente.apellidos
      }
    })
  }
}
