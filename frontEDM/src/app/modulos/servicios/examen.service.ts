import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {ExamenInterface} from '../interfaces/examen-interface';

@Injectable()
export class ExamenRestService {
  constructor(
    private readonly _http: HttpClient
  ) {
  }

  findAll(skip: number, take: number, relations: number) {
    return this._http.get('http://localhost:3000/examen?skip=' + skip + '&take=' + take);
  }

  findAllPorId(skip: number, take: number, idCliente: number) {
    return this._http.get('http://localhost:3000/examen?skip=' + skip + '&take=' + take + '&idCliente=' + idCliente);
  }

  createOne(examen: ExamenInterface, clienteId: number) {

    return this._http.post('http://localhost:3000/examen', {
      examen: {
        fecha: examen.fecha,
        comentario: examen.comentario,
        tipoExamen: examen.tipoExamen,
        cliente: +clienteId
      }

    });
  }

  updateOne(idExamen: number, examen: ExamenInterface) {
    return this._http.put('http://localhost:3000/examen', {
      examen: {
        fecha: examen.fecha,
        comentario: examen.comentario,
        tipoExamen: examen.tipoExamen,
        id: idExamen
      }
    })
  }

  remove(idExamen: number) {
    return this._http.delete('http://localhost:3000/examen?id=' + idExamen);
  }

  buscarPorFecha(fecha: string, idCliente: number) {
    return this._http.get('http://localhost:3000/examen/filtroFecha?fecha=' + fecha + '&idCliente=' + idCliente);
  }
}
