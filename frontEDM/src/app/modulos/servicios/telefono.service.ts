import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {TelefonoInterface} from '../interfaces/telefono-interface';

@Injectable()
export class TelefonoRestservice {
  constructor(
    private readonly _http: HttpClient
  ) {
  }

  findAll(skip: number, take: number, relations: number, idCliente: number) {
    return this._http.get('http://localhost:3000/telefono?skip=' + skip + '&take=' + take + '&relations=' + relations);
  }

  findAllPorId(skip: number, take: number, idCliente: number) {
    return this._http.get('http://localhost:3000/telefono?skip=' + skip + '&take=' + take + '&idCliente=' + idCliente);
  }

  createOne(telefono: string, clienteId: number) {

    return this._http.post('http://localhost:3000/telefono', {
      telefono: {
        numero: telefono,
        cliente: clienteId
      }

    });
  }

  updateOne(telefono: TelefonoInterface, idCliente: number) {
    return this._http.put('http://localhost:3000/telefono', {
      telefono: {
        id: idCliente,
        numero: telefono.numero
      }
    })
  }

  remove(idTelefono: number) {
    return this._http.delete('http://localhost:3000/telefono?id='+idTelefono)
  }
}
