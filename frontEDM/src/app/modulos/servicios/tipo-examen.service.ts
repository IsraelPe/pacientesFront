import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {TipoExamenInterface} from '../interfaces/tipo-examen.interface';

@Injectable()
export class TipoExamenRestService {
  constructor(
    private readonly _http: HttpClient
  ) {
  }

  findAll(skip?: number, take?: number) {
    if (skip && take) {
      return this._http.get('http://localhost:3000/tipo-examen?skip=' + skip + '&take=' + take);
    } else {
      return this._http.get('http://localhost:3000/tipo-examen');

    }

  }

  findLike(busquedaLike: string, skip?: number, take?: number) {
    if (!skip && !take) {
      return this._http.get('http://localhost:3000/tipo-examen?busquedaLike=' + busquedaLike);

    } else {
      return this._http.get('http://localhost:3000/tipo-examen?busquedaLike=' + busquedaLike + '&skip' + skip + '&take=' + take);
    }
  }

  findAllPorId(idTipoExamen: number) {
    return this._http.get('http://localhost:3000/tipo-examen?id=' + idTipoExamen);
  }

  createOne(tipoExamen: TipoExamenInterface) {

    return this._http.post('http://localhost:3000/tipo-examen', {
      tipoExamen
    });
  }

  updateOne(tipoExamen: TipoExamenInterface) {
    return this._http.put('http://localhost:3000/tipo-examen', {
      tipoExamen
    })
  }

  remove(idTipoExamen: number) {
    return this._http.delete('http://localhost:3000/tipo-examen?id=' + idTipoExamen)
  }
}
