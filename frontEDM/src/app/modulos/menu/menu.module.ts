import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MenuComponent } from './menu.component';
import {MenuRoutingModule} from './menu-routing.module';
import {TituloModule} from '../../componentes-generales/titulo/titulo.module';



@NgModule({
  declarations: [MenuComponent],
  imports: [
    CommonModule,
    MenuRoutingModule,
    TituloModule
  ]
})
export class MenuModule { }
