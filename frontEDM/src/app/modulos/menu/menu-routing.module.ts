import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {MenuComponent} from './menu.component';

export const rutasExamen: Routes = [
  {
    path: 'menu-principal',
    component: MenuComponent
  },
  {
    path: 'cliente',
    loadChildren: () => import('./../cliente/cliente.module').then(modulo => modulo.ClienteModule)
  },
  {
    path: 'tipo-examen',
    loadChildren: () => import('./../tipo-examen/tipo-examen.module').then(modulo => modulo.TipoExamenModule)
  },

];

@NgModule({
  imports: [RouterModule.forChild(rutasExamen)]
})
export class MenuRoutingModule {

}
