import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TipoExamenComponent} from './tipo-examen.component';
import {BreadcrumbModule, TableModule} from 'primeng';
import {FormBusquedaModule} from '../../componentes-generales/form-busqueda/form-busqueda.module';
import {TituloModule} from '../../componentes-generales/titulo/titulo.module';
import {TipoExamenRoutingModule} from './tipo-examen-routing.module';
import {MatDialogModule} from '@angular/material/dialog';
import {ModalCrearEditarTipoExamenModule} from './modales/modal-crear-editar-tipo-examen/modal-crear-editar-tipo-examen.module';


@NgModule({
  declarations: [TipoExamenComponent],
  imports: [
    CommonModule,
    TableModule,
    FormBusquedaModule,
    BreadcrumbModule,
    TituloModule,
    TipoExamenRoutingModule,
    MatDialogModule,
    ModalCrearEditarTipoExamenModule
  ]
})
export class TipoExamenModule {
}
