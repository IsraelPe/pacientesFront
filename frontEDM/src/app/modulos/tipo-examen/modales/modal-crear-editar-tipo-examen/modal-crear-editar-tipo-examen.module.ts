import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModalCrearEditarTipoExamenComponent } from './modal-crear-editar-tipo-examen.component';
import {MatDialogModule} from '@angular/material/dialog';
import {FormularioTipoExamenModule} from '../../formularios/formulario-tipo-examen/formulario-tipo-examen.module';



@NgModule({
  declarations: [ModalCrearEditarTipoExamenComponent],
  imports: [
    CommonModule,
    MatDialogModule,
    FormularioTipoExamenModule
  ]
})
export class ModalCrearEditarTipoExamenModule { }
