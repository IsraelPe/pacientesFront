import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {ClienteComponent} from '../../../cliente/cliente.component';
import {ClientesRestService} from '../../../servicios/cliente.service';
import {TipoExamenInterface} from '../../../interfaces/tipo-examen.interface';
import {TipoExamenRestService} from '../../../servicios/tipo-examen.service';

@Component({
  selector: 'app-modal-crear-editar-tipo-examen',
  templateUrl: './modal-crear-editar-tipo-examen.component.html',
  styleUrls: ['./modal-crear-editar-tipo-examen.component.css']
})
export class ModalCrearEditarTipoExamenComponent implements OnInit {

  habilitarGuardar = false;
  tipoExamenAGuardar: TipoExamenInterface;
  datosTipoExamenEditar = this.data

  constructor(private readonly _dialogRef: MatDialogRef<ClienteComponent>,
              protected _clienteService: ClientesRestService,
              protected _tipoExamenService: TipoExamenRestService,
              @Inject(MAT_DIALOG_DATA) private  data: TipoExamenInterface
  ) {
  }

  ngOnInit(): void {
  }

  capturarDatosTelefono(evento: TipoExamenInterface | boolean) {
    if (typeof evento === 'boolean') {
      this.habilitarGuardar = evento;
    } else {
      this.habilitarGuardar = true;
      this.tipoExamenAGuardar = evento;
    }
  }

  enviarDatos() {
    if (!this.data) {
      this._tipoExamenService.createOne(this.tipoExamenAGuardar)
        .subscribe(
          res => {
            this._dialogRef.close(res);
          },
          error => console.error('Error creando tipo examen...', error)
        )
    } else {
      if (this.tipoExamenAGuardar !== undefined) {
        this.tipoExamenAGuardar.id = this.data.id;
        this._tipoExamenService.updateOne(this.tipoExamenAGuardar)
          .subscribe(
            res => {
              const telefonoEditado = this.tipoExamenAGuardar
              telefonoEditado.id = this.data.id;
              this._dialogRef.close(telefonoEditado)
            },
            error => {
              console.error('Error actualizado telefono...', error)
            }
          )
      }
    }

  }

}
