import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalCrearEditarTipoExamenComponent } from './modal-crear-editar-tipo-examen.component';

describe('ModalCrearEditarTipoExamenComponent', () => {
  let component: ModalCrearEditarTipoExamenComponent;
  let fixture: ComponentFixture<ModalCrearEditarTipoExamenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalCrearEditarTipoExamenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalCrearEditarTipoExamenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
