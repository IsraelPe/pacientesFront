import {Component, OnInit} from '@angular/core';
import {LazyLoadEvent, MenuItem} from 'primeng';
import {MatDialog} from '@angular/material/dialog';
import {Router} from '@angular/router';
import {TipoExamenRestService} from '../servicios/tipo-examen.service';
import {TipoExamenInterface} from '../interfaces/tipo-examen.interface';
import {ModalCrearEditarTipoExamenComponent} from './modales/modal-crear-editar-tipo-examen/modal-crear-editar-tipo-examen.component';

@Component({
  selector: 'app-tipo-examen',
  templateUrl: './tipo-examen.component.html',
  styleUrls: ['./tipo-examen.component.css']
})
export class TipoExamenComponent implements OnInit {

  columnasCliente = [
    {
      header: "Nombre",
      campo: "nombre"
    },
    {
      header: "Opciones"
    }
  ]
  filasTablaTipoExamen = 10;
  busquedaTipoExamen;
  cantidadTipoExamen = 0;
  datosTipoExamen: TipoExamenInterface[] = [];
  cargando = false;

  migasDePan: MenuItem[];

  constructor(
    protected _tipoExamenService: TipoExamenRestService,
    public dialog: MatDialog,
    private readonly router: Router
  ) {
  }

  ngOnInit() {
    this.migasDePan = [
      {label: 'Menú principal', routerLink: '/'},
      {label: 'Tipos de Exámenes'}
    ]
    this.cargarDatosLazy();
  }

  cargarDatosLazy(event?: LazyLoadEvent) {
    this.cargando = true;
    if (event === undefined) {
      this._tipoExamenService.findAll(this.filasTablaTipoExamen, 1).subscribe(
        examenes => {
          this.datosTipoExamen = examenes[0];
          this.cantidadTipoExamen = examenes[1];
          this.cargando = false;
        },
        error => {
          console.error('Error trayendo datos...', error);
          this.cargando = false;
        }
      )
    } else {
      this._tipoExamenService.findAll(event.first, (event.first + event.rows)).subscribe(
        examenes => {
          this.datosTipoExamen = examenes[0];
          this.cantidadTipoExamen = examenes[1];
          this.cargando = false;
        },
        error => {
          console.error('Error trayendo datos...', error);
          this.cargando = false;
        }
      )
    }


  }

  abrirModalCrearEditarTipoExamen(tipoExamen?: TipoExamenInterface) {
    const dialogRef = this.dialog.open(ModalCrearEditarTipoExamenComponent, {
      height: '300px',
      width: '600px',
      data: tipoExamen,
    });

    const resultadoModal$ = dialogRef.afterClosed();
    resultadoModal$.subscribe((registroRes) => {
        if (tipoExamen && registroRes) {
          const indiceCliente = this.datosTipoExamen.findIndex(datos => tipoExamen.id === datos.id);
          this.datosTipoExamen[indiceCliente] = registroRes;
        } else {
          this.cargarDatosLazy();
        }
      },
      error => {
        console.error('Error...')
      }
    );
  }

  escucharBusqueda(evento) {
    this.busquedaTipoExamen = evento.busqueda;
  }

  buscarTipoExamen() {
    this._tipoExamenService.findLike(this.busquedaTipoExamen, 0, 10)
      .subscribe(
        tipoExamen => {
          this.datosTipoExamen = tipoExamen[0];
          this.cantidadTipoExamen = tipoExamen[1];
        },
        error => console.error('Error trayendo datos...', error)
      )
  }

}
