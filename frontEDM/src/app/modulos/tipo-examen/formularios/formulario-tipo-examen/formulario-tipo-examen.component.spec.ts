import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormularioTipoExamenComponent } from './formulario-tipo-examen.component';

describe('FormularioTipoExamenComponent', () => {
  let component: FormularioTipoExamenComponent;
  let fixture: ComponentFixture<FormularioTipoExamenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormularioTipoExamenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormularioTipoExamenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
