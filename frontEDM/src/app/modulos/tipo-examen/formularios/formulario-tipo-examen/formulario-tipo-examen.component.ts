import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {debounceTime} from 'rxjs/operators';
import {TipoExamenInterface} from '../../../interfaces/tipo-examen.interface';

@Component({
  selector: 'app-formulario-tipo-examen',
  templateUrl: './formulario-tipo-examen.component.html',
  styleUrls: ['./formulario-tipo-examen.component.css']
})
export class FormularioTipoExamenComponent implements OnInit {

  @Input() datosTipoExamen: TipoExamenInterface;
  @Output() TipoExamen: EventEmitter<Object | boolean> = new EventEmitter<Object | boolean>();
  formularioTipoExamen: FormGroup;

  constructor() {
    this.formularioTipoExamen = new FormGroup(
      {
        nombre: new FormControl('',
          [
            Validators.maxLength(120),
            Validators.minLength(2),
            Validators.pattern('[a-zA-ZñÑáéíóúÁÉÍÓÚ ]*'),
          ]
        )
      }
    )
  }

  ngOnInit(): void {
    if (this.datosTipoExamen) {
      this.formularioTipoExamen.patchValue(this.datosTipoExamen);
    }
    this.escucharFormulario();
  }

  escucharFormulario() {
    this.formularioTipoExamen
      .valueChanges
      .pipe(
        debounceTime(200)
      )
      .subscribe(
        valoresFormulario => {
          const esFormularioValido = this.formularioTipoExamen.valid;
          if (!esFormularioValido) {
            this.TipoExamen.emit(false);
          } else {
            this.TipoExamen.emit(valoresFormulario);
          }

        }
      )
  }


}
