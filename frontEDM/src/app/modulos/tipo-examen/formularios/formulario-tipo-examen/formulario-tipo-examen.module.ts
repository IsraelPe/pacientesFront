import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormularioTipoExamenComponent } from './formulario-tipo-examen.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';



@NgModule({
  declarations: [FormularioTipoExamenComponent],
  exports: [
    FormularioTipoExamenComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule
  ]
})
export class FormularioTipoExamenModule { }
