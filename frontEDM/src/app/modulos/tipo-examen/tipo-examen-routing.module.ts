import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {TipoExamenComponent} from './tipo-examen.component';

export const rutasExamen: Routes = [
  {
    path: 'gestion-tipo-examen',
    component: TipoExamenComponent
  }

];

@NgModule({
  imports: [RouterModule.forChild(rutasExamen)]
})
export class TipoExamenRoutingModule {

}
