import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NoEncontradoComponent } from './no-encontrado.component';



@NgModule({
  declarations: [NoEncontradoComponent],
  imports: [
    CommonModule
  ]
})
export class NoEncontradoModule { }
