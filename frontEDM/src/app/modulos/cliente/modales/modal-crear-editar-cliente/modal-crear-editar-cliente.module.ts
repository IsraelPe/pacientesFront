import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ModalCrearEditarClienteComponent} from './modal-crear-editar-cliente.component';
import {MatDialogModule} from '@angular/material/dialog';
import {FormClienteModule} from '../../formularios/form-cliente/form-cliente.module';


@NgModule({
  declarations: [
    ModalCrearEditarClienteComponent
  ],
  imports: [
    CommonModule,
    MatDialogModule,
    FormClienteModule
  ]
})
export class ModalCrearEditarClienteModule {
}
