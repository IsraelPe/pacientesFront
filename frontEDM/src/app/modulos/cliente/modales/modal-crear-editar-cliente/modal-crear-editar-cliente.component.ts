import {Component, Inject, OnInit} from '@angular/core';
import {ClienteInterface} from '../../../interfaces/cliente-interface';
import {ClientesRestService} from '../../../servicios/cliente.service';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {ClienteComponent} from '../../cliente.component';
import {TelefonoRestservice} from '../../../servicios/telefono.service';
import {TelefonoInterface} from '../../../interfaces/telefono-interface';

@Component({
  selector: 'app-modal-crear-editar-cliente',
  templateUrl: './modal-crear-editar-cliente.component.html',
  styleUrls: ['./modal-crear-editar-cliente.component.css']
})
export class ModalCrearEditarClienteComponent implements OnInit {
  habilitarGuardar = false;
  clienteAGuardar: ClienteInterface;
  datosClienteEditar = this.data

  constructor(private readonly _dialogRef: MatDialogRef<ClienteComponent>,
              protected _clienteService: ClientesRestService,
              protected _telefonoService: TelefonoRestservice,
              @Inject(MAT_DIALOG_DATA) private  data: ClienteInterface
  ) {
  }

  ngOnInit(): void {
  }

  capturarDatosCliente(evento: ClienteInterface | boolean) {
    if (typeof evento === 'boolean') {
      this.habilitarGuardar = evento;
    } else {
      this.habilitarGuardar = true;
      this.clienteAGuardar = evento;
    }
  }

  enviarDatos() {
    if (!this.data) {
      this._clienteService.createOne(this.clienteAGuardar)
        .subscribe(
          res => {
            const respuesta: ClienteInterface = res;
            if (this.clienteAGuardar.telefono !== '' && typeof this.clienteAGuardar.telefono === 'string') {
              this._telefonoService.createOne(this.clienteAGuardar.telefono, respuesta.id)
                .subscribe(
                  resTel => {
                    const telefonoCreado: TelefonoInterface = resTel;
                    res["telefonos"] = [{numero: telefonoCreado.numero}]
                    this._dialogRef.close(res)
                  },
                  error => console.error('error creando el telefono :,c', error)
                )
            } else {
              res["telefonos"] = []
              this._dialogRef.close(res)
            }
          },
          error => console.error('error creando cliente', error)
        )
    } else {
      if (this.clienteAGuardar !== undefined) {
        this._clienteService.updateOne(this.data.id, this.clienteAGuardar)
          .subscribe(
            res => {
              const clienteEditado = this.clienteAGuardar
              clienteEditado['telefonos'] = this.data.telefonos
              this._dialogRef.close(clienteEditado)
            },
            error => {
              console.error('Error actualizado cliente...', error)
            }
          )
      }
    }

  }
}
