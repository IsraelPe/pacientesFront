import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalCrearEditarClienteComponent } from './modal-crear-editar-cliente.component';

describe('ModalCrearEditarClienteComponent', () => {
  let component: ModalCrearEditarClienteComponent;
  let fixture: ComponentFixture<ModalCrearEditarClienteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalCrearEditarClienteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalCrearEditarClienteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
