import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormClienteComponent} from './form-cliente.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';


@NgModule({
  declarations: [
    FormClienteComponent,

  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule
  ],
  exports: [
    FormClienteComponent
  ]
})
export class FormClienteModule {
}
