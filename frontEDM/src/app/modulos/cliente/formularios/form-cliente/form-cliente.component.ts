import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {debounceTime} from 'rxjs/operators';
import {ClienteInterface} from '../../../interfaces/cliente-interface';

@Component({
  selector: 'app-form-cliente',
  templateUrl: './form-cliente.component.html',
  styleUrls: ['./form-cliente.component.css']
})
export class FormClienteComponent implements OnInit {

  @Input() datosCliente: ClienteInterface;
  @Output() Cliente: EventEmitter<Object | boolean> = new EventEmitter<Object | boolean>();
  formularioCliente: FormGroup;
  ocultarTelefono = false;

  constructor() {
    this.formularioCliente = new FormGroup(
      {
        nombres: new FormControl('', [
            Validators.required,
            Validators.maxLength(60),
            Validators.minLength(2),
            Validators.pattern('[a-zA-ZñÑáéíóúÁÉÍÓÚ ]*'),
          ]
        ),
        apellidos: new FormControl('', [
            Validators.required,
            Validators.maxLength(60),
            Validators.minLength(2),
            Validators.pattern('[a-zA-ZñÑáéíóúÁÉÍÓÚ ]*'),
          ]
        ),
        telefono: new FormControl('',
          [
            Validators.maxLength(12),
            Validators.minLength(7),
            Validators.pattern('[0-9]*'),
          ]
        )
      }
    )
  }

  ngOnInit(): void {
    if (this.datosCliente) {
      this.formularioCliente.patchValue(this.datosCliente);
      this.ocultarTelefono = true;
    }
    this.escucharFormulario();
  }

  escucharFormulario() {
    this.formularioCliente
      .valueChanges
      .pipe(
        debounceTime(200)
      )
      .subscribe(
        valoresFormulario => {
          const esFormularioValido = this.formularioCliente.valid;
          if (!esFormularioValido) {
            this.Cliente.emit(false);
          } else {
            this.Cliente.emit(valoresFormulario);
          }

        }
      )
  }
}
