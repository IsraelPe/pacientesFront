import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ClienteComponent} from './cliente.component';
import {ClienteRoutingModule} from './cliente-routing.module';
import {TituloModule} from '../../componentes-generales/titulo/titulo.module';
import {BreadcrumbModule, TableModule} from 'primeng';
import {MatDialogModule} from '@angular/material/dialog';
import {ModalCrearEditarClienteModule} from './modales/modal-crear-editar-cliente/modal-crear-editar-cliente.module';
import {ModalCrearEditarClienteComponent} from './modales/modal-crear-editar-cliente/modal-crear-editar-cliente.component';
import {RouterModule} from '@angular/router';
import {FormBusquedaModule} from '../../componentes-generales/form-busqueda/form-busqueda.module';
import {FormBusquedaComponent} from '../../componentes-generales/form-busqueda/form-busqueda.component';


@NgModule({
  declarations: [
    ClienteComponent
  ],
  imports: [
    CommonModule,
    ClienteRoutingModule,
    TituloModule,
    TableModule,
    MatDialogModule,
    ModalCrearEditarClienteModule,
    RouterModule,
    FormBusquedaModule,
    BreadcrumbModule

  ],
  entryComponents: [
    FormBusquedaComponent,
    ModalCrearEditarClienteComponent
  ],
  providers: []
})
export class ClienteModule {
}
