import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {ClienteComponent} from './cliente.component';

export const rutasCliente: Routes = [
  {
    path: 'gestion-cliente',
    component: ClienteComponent,
  },
  {
    path: ':idCliente/telefono',
    loadChildren: () => import('./../telefono/telefono.module').then(modulo => modulo.TelefonoModule),

  },
  {
    path: ':idCliente/examen',
    loadChildren: () => import('./../examen/examen.module').then(modulo => modulo.ExamenModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(rutasCliente)]
})
export class ClienteRoutingModule {

}
