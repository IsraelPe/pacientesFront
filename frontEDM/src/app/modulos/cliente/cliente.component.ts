import {Component, OnInit} from '@angular/core';
import {ClientesRestService} from '../servicios/cliente.service';
import {ClienteInterface} from '../interfaces/cliente-interface';
import {LazyLoadEvent, MenuItem} from 'primeng';
import {ModalCrearEditarClienteComponent} from './modales/modal-crear-editar-cliente/modal-crear-editar-cliente.component';
import {MatDialog} from '@angular/material/dialog';
import {Router} from '@angular/router';

@Component({
  selector: 'app-cliente',
  templateUrl: './cliente.component.html',
  styleUrls: ['./cliente.component.css']
})
export class ClienteComponent implements OnInit {
  columnasCliente = [
    {
      header: "Apellidos",
      campo: "apellidos"
    },
    {
      header: "Nombres",
      campo: "nombres"
    },
    {
      header: "Teléfonos",
      campo: "telefonos"
    },
    {
      header: "Opciones"
    }
  ]
  filasTablaClientes = 10;
  busquedaCliente;
  cantidadClientes = 0;
  datosClientes: ClienteInterface[] = [];
  cargando = false;

  migasDePan: MenuItem[];

  constructor(
    protected _clienteService: ClientesRestService,
    public dialog: MatDialog,
    private readonly router: Router
  ) {
  }

  ngOnInit() {
    this.migasDePan = [
      {label: 'Menú principal', routerLink: '/'},
      {label: 'Pacientes'}
    ]
    this.cargarDatosLazy();
  }

  cargarDatosLazy(event?: LazyLoadEvent) {
    this.cargando = true;
    if (event === undefined) {
      this._clienteService.findAll(0, this.filasTablaClientes, 1).subscribe(
        clientes => {
          this.datosClientes = clientes[0];
          this.cantidadClientes = clientes[1];
          this.cargando = false;
        },
        error => {
          console.error('Error trayendo datos...', error);
          this.cargando = false;
        }
      )
    } else {
      this._clienteService.findAll(event.first, (event.first + event.rows), 1).subscribe(
        clientes => {
          this.datosClientes = clientes[0];
          this.cantidadClientes = clientes[1];
          this.cargando = false;
        },
        error => {
          console.error('Error trayendo datos...', error);
          this.cargando = false;
        }
      )
    }


  }

  abrirModalCrearEditarCliente(cliente?: ClienteInterface) {
    const dialogRef = this.dialog.open(ModalCrearEditarClienteComponent, {
      height: '500px',
      width: '600px',
      data: cliente,
    });

    const resultadoModal$ = dialogRef.afterClosed();
    resultadoModal$.subscribe((registroRes) => {
        if (cliente && registroRes) {
          const indiceCliente = this.datosClientes.findIndex(datos => cliente.id === datos.id);
          this.datosClientes[indiceCliente] = registroRes;
        } else {
          this.cargarDatosLazy();
        }
      },
      error => {
        console.error('Error...')
      }
    );
  }

  escucharBusqueda(evento) {
    this.busquedaCliente = evento.busqueda;
  }

  buscarCliente() {
    this._clienteService.findAll(0, 10, 1, this.busquedaCliente)
      .subscribe(
        clientes => {
          this.datosClientes = clientes[0];
          this.cantidadClientes = clientes[1];
        },
        error => console.error('Error trayendo datos...', error)
      )
  }

  irAExamenesCliente(idCliente: number) {
    this.router.navigate([`/menu/cliente/${idCliente}/examen/gestion-examen`]);
  }

  irATelefonosCliente(idCliente: number) {
    this.router.navigate([`/menu/cliente/${idCliente}/telefono/gestion-telefono`]);
  }

}
