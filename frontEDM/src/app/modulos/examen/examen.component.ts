import {Component, OnInit} from '@angular/core';
import {TelefonoInterface} from '../interfaces/telefono-interface';
import {ActivatedRoute} from '@angular/router';
import {ClientesRestService} from '../servicios/cliente.service';
import {MatDialog} from '@angular/material/dialog';
import {LazyLoadEvent, MenuItem} from 'primeng';
import {ClienteInterface} from '../interfaces/cliente-interface';
import {ExamenInterface} from '../interfaces/examen-interface';
import {ExamenRestService} from '../servicios/examen.service';
import {ModalCrearEditarExamenComponent} from './modales/modal-crear-editar-examen/modal-crear-editar-examen.component';

@Component({
  selector: 'app-examen',
  templateUrl: './examen.component.html',
  styleUrls: ['./examen.component.css']
})
export class ExamenComponent implements OnInit {
  idCliente: number;
  filasTablaExamen = 5;
  datosExamen: ExamenInterface[];
  cantidadExamenes: number;
  nombreCliente: string;
  fechaBusqueda: string;
  columnasExamen = [
    {
      header: "Tipo Examen",
    },
    {
      header: "Comentario",
      campo: "comentario"
    },
    {
      header: "Fecha",
      campo: "fecha"
    },
    {
      header: "Opciones"
    }
  ]
  migasDePan: MenuItem[];

  constructor(
    private readonly _activatedRoute: ActivatedRoute,
    private readonly _examenService: ExamenRestService,
    private readonly _clienteService: ClientesRestService,
    public dialog: MatDialog,
  ) {
  }

  ngOnInit(): void {
    this.migasDePan = [
      {label: 'Menú principal', routerLink: '/'},
      {label: 'Pacientes', routerLink: `/menu/cliente/gestion-cliente`},
      {label: 'Exámenes'}
    ]
    this._activatedRoute.params.subscribe(
      (parametros: { idCliente: number }) => {
        this.idCliente = parametros.idCliente;
        this.cargarNombreCliente();
        this.cargarDatosLazy();
      }
    )

  }

  cargarDatosLazy(event?: LazyLoadEvent) {
    if (event === undefined) {
      this._examenService.findAllPorId(0, this.filasTablaExamen, this.idCliente)
        .subscribe(
          examen => {
            this.datosExamen = examen[0];
            this.cantidadExamenes = examen[1];
          },
          error => console.error('Error trayendo datos...', error)
        )
    } else {
      this._examenService.findAllPorId(event.first, (event.first + event.rows), this.idCliente)
        .subscribe(
          examen => {
            this.datosExamen = examen[0];
            this.cantidadExamenes = examen[1];
          },
          error => console.error('Error trayendo datos...', error)
        )
    }
  }

  cargarNombreCliente() {
    this._clienteService.findOne(this.idCliente).subscribe(
      (res: ClienteInterface) => this.nombreCliente = res.apellidos + ' ' + res.nombres
    )
  }

  eliminarTelefono(fila: TelefonoInterface) {
    return this._examenService.remove(fila.id).subscribe(
      res => {
        this.cargarDatosLazy()
      }
    );
  }

  abrirModalCrearEditarExamen(examen?: ExamenInterface) {
    const dialogRef = this.dialog.open(ModalCrearEditarExamenComponent, {
      height: '600px',
      width: '600px',
      data: {examen, idCliente: this.idCliente}
    });

    const resultadoModal$ = dialogRef.afterClosed();
    resultadoModal$.subscribe((registroRes) => {
        this.cargarDatosLazy();
      },
      error => {
        console.error('Error...')
      }
    );
  }

  escucharFechaBusqueda(event) {
    this.fechaBusqueda = event.busqueda;
    if (this.fechaBusqueda !== '') {
      this._examenService.buscarPorFecha(this.fechaBusqueda, this.idCliente)
        .subscribe(
          examen => {
            this.datosExamen = examen[0];
            this.cantidadExamenes = examen[1];
          },
          error => console.error('Error trayendo datos...', error)
        )
    } else {
      this.cargarDatosLazy();
    }
  }


}
