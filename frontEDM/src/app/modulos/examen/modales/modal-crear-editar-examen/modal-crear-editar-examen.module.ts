import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ModalCrearEditarExamenComponent} from './modal-crear-editar-examen.component';
import {MatDialogModule} from '@angular/material/dialog';
import {FormularioExamenModule} from '../../formularios/formulario-examen/formulario-examen.module';


@NgModule({
  declarations: [
    ModalCrearEditarExamenComponent
  ],
  imports: [
    CommonModule,
    MatDialogModule,
    FormularioExamenModule
  ]
})
export class ModalCrearEditarExamenModule {
}
