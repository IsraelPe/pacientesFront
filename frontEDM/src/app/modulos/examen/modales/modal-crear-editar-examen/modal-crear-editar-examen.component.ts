import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {ClienteComponent} from '../../../cliente/cliente.component';
import {TelefonoRestservice} from '../../../servicios/telefono.service';
import {ExamenInterface} from '../../../interfaces/examen-interface';
import {ExamenRestService} from '../../../servicios/examen.service';
import {TipoExamenRestService} from '../../../servicios/tipo-examen.service';

@Component({
  selector: 'app-modal-crear-editar-examen',
  templateUrl: './modal-crear-editar-examen.component.html',
  styleUrls: ['./modal-crear-editar-examen.component.css']
})
export class ModalCrearEditarExamenComponent implements OnInit {

  habilitarGuardar = false;
  examenAGuardar: ExamenInterface;
  datosExamenEditar = this.data

  constructor(private readonly _dialogRef: MatDialogRef<ClienteComponent>,
              protected _examenService: ExamenRestService,
              protected _telefonoService: TelefonoRestservice,
              protected _tipoExamenService: TipoExamenRestService,
              @Inject(MAT_DIALOG_DATA) private  data: { examen: ExamenInterface, idCliente: number }
  ) {
  }

  ngOnInit(): void {
  }

  capturarDatosExamen(evento: ExamenInterface | boolean) {
    if (typeof evento === 'boolean') {
      this.habilitarGuardar = evento;
    } else {
      this.habilitarGuardar = true;
      this.examenAGuardar = evento;
      if (typeof this.examenAGuardar.tipoExamen === 'object') {
        this.examenAGuardar.tipoExamen = this.examenAGuardar.tipoExamen.id
      }

    }
  }

  enviarDatos() {
    if (!this.data.examen) {
      if (this.examenAGuardar.comentario === '') {
        delete this.examenAGuardar['comentario'];
      }
      this._examenService.createOne(this.examenAGuardar, this.data.idCliente)
        .subscribe(
          res => {
            this._dialogRef.close(this.examenAGuardar)
          },
          error => console.error('Error creando examen...', error)
        )
    } else {
      if (this.examenAGuardar !== undefined) {
        console.log("entro en la actualizacion: ", this.examenAGuardar);
        this._examenService.updateOne(this.data.examen.id, this.examenAGuardar)
          .subscribe(
            res => {
              const examenEditado = this.examenAGuardar
              examenEditado.id = this.data.examen.id
              this._dialogRef.close(examenEditado)
            },
            error => {
              console.error('Error actualizado examen...', error)
            }
          )
      }
    }

  }

}
