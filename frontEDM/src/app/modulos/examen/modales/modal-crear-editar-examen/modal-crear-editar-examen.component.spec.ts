import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalCrearEditarExamenComponent } from './modal-crear-editar-examen.component';

describe('ModalCrearEditarExamenComponent', () => {
  let component: ModalCrearEditarExamenComponent;
  let fixture: ComponentFixture<ModalCrearEditarExamenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalCrearEditarExamenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalCrearEditarExamenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
