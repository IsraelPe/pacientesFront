import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ExamenComponent} from './examen.component';
import {ExamenRoutingModule} from './examen-routing.module';
import {TituloModule} from '../../componentes-generales/titulo/titulo.module';
import {BreadcrumbModule, TableModule} from 'primeng';
import {MatDialogModule} from '@angular/material/dialog';
import {ModalCrearEditarExamenModule} from './modales/modal-crear-editar-examen/modal-crear-editar-examen.module';
import {FiltroFechaModule} from '../../componentes-generales/filtro-fecha/filtro-fecha.module';


@NgModule({
  declarations: [
    ExamenComponent
  ],
  imports: [
    CommonModule,
    ExamenRoutingModule,
    TituloModule,
    TableModule,
    MatDialogModule,
    ModalCrearEditarExamenModule,
    BreadcrumbModule,
    FiltroFechaModule
  ]
})
export class ExamenModule {
}
