import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormularioExamenComponent} from './formulario-examen.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AutoCompleteModule} from 'primeng';


@NgModule({
  declarations: [
    FormularioExamenComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    AutoCompleteModule
  ],
  exports: [
    FormularioExamenComponent
  ]
})
export class FormularioExamenModule {
}
