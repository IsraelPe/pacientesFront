import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {debounceTime} from 'rxjs/operators';
import {ExamenInterface} from '../../../interfaces/examen-interface';
import {TipoExamenRestService} from '../../../servicios/tipo-examen.service';
import {TipoExamenInterface} from '../../../interfaces/tipo-examen.interface';

@Component({
  selector: 'app-formulario-examen',
  templateUrl: './formulario-examen.component.html',
  styleUrls: ['./formulario-examen.component.css']
})
export class FormularioExamenComponent implements OnInit {

  @Input() datosExamen: { examen: ExamenInterface, idClieente: number };
  @Output() Examen: EventEmitter<Object | boolean> = new EventEmitter<Object | boolean>();
  formularioExamen: FormGroup;
  tiposExamen: TipoExamenInterface[];

  constructor(
    protected _tipoExamenService: TipoExamenRestService,
  ) {
    this.formularioExamen = new FormGroup(
      {
        tipoExamen: new FormControl('', [
            Validators.required,
          ]
        ),
        fecha: new FormControl('', [
            Validators.required,
          ]
        ),
        comentario: new FormControl('',
          [
            Validators.maxLength(255),
            Validators.minLength(5),
            Validators.pattern('[0-9a-zA-ZñÑáéíóúÁÉÍÓÚ ]*'),
          ]
        )
      }
    )
  }

  ngOnInit(): void {
    if (this.datosExamen.examen) {
      this.formularioExamen.patchValue(this.datosExamen.examen);
    }
    this.cargarTipoExamen();
    this.escucharFormulario();
  }

  escucharFormulario() {
    this.formularioExamen
      .valueChanges
      .pipe(
        debounceTime(200)
      )
      .subscribe(
        valoresFormulario => {
          const esFormularioValido = this.formularioExamen.valid;
          if (!esFormularioValido) {
            this.Examen.emit(false);
          } else {
            this.Examen.emit(valoresFormulario);
          }

        }
      )
  }

  cargarTipoExamen(event?) {
    if (event === undefined) {
      this._tipoExamenService.findAll().subscribe(
        res => {
          this.tiposExamen = res[0]
        }
      )
    } else {
      this._tipoExamenService.findLike(event.query).subscribe(
        res => {
          this.tiposExamen = res[0]
        }
      )
    }

  }
}
