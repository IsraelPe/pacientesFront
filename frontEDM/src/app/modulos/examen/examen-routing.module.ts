import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {ExamenComponent} from './examen.component';

export const rutasExamen: Routes = [
  {
    path: 'gestion-examen',
    component: ExamenComponent
  }

];

@NgModule({
  imports: [RouterModule.forChild(rutasExamen)]
})
export class ExamenRoutingModule {

}
