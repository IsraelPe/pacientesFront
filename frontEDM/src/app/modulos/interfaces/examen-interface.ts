import {TipoExamenInterface} from './tipo-examen.interface';

export interface ExamenInterface {
  id?: number,
  comentario?: string,
  tipoExamen?: TipoExamenInterface | number;
  fecha?: string,
}
