import {TelefonoInterface} from './telefono-interface';

export interface ClienteInterface {
  id?: number,
  nombres?: string,
  apellidos?: string,
  telefonos?: string | TelefonoInterface[],
  telefono?: string,
  idCliente?: string,
}
