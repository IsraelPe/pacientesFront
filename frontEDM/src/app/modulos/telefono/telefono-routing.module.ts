import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {TelefonoComponent} from './telefono.component';

export const rutasTelefono: Routes = [
  {
    path: 'gestion-telefono',
    component: TelefonoComponent,
  }

];

@NgModule({
  imports: [RouterModule.forChild(rutasTelefono)]
})
export class TelefonoRoutingModule {

}
