import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TelefonoComponent} from './telefono.component';
import {TelefonoRoutingModule} from './telefono-routing.module';
import {TituloModule} from '../../componentes-generales/titulo/titulo.module';
import {BreadcrumbModule, TableModule} from 'primeng';
import {MatDialogModule} from '@angular/material/dialog';
import {CrearEditarTelefonoModule} from './modales/crear-editar-telefono/crear-editar-telefono.module';
import {CrearEditarTelefonoComponent} from './modales/crear-editar-telefono/crear-editar-telefono.component';


@NgModule({
  declarations: [
    TelefonoComponent
  ],
  imports: [
    CommonModule,
    TelefonoRoutingModule,
    TituloModule,
    TableModule,
    MatDialogModule,
    CrearEditarTelefonoModule,
    BreadcrumbModule
  ],
  entryComponents: [
    CrearEditarTelefonoComponent
  ]
})
export class TelefonoModule {
}
