import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CrearEditarTelefonoComponent} from './crear-editar-telefono.component';
import {FormularioTelefonoModule} from '../../formularios/formulario-telefono/formulario-telefono.module';
import {MatDialogModule} from '@angular/material/dialog';


@NgModule({
  declarations: [CrearEditarTelefonoComponent],
  imports: [
    CommonModule,
    MatDialogModule,
    FormularioTelefonoModule,
  ]
})
export class CrearEditarTelefonoModule {
}
