import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CrearEditarTelefonoComponent } from './crear-editar-telefono.component';

describe('CrearEditarTelefonoComponent', () => {
  let component: CrearEditarTelefonoComponent;
  let fixture: ComponentFixture<CrearEditarTelefonoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrearEditarTelefonoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CrearEditarTelefonoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
