import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {ClienteComponent} from '../../../cliente/cliente.component';
import {ClientesRestService} from '../../../servicios/cliente.service';
import {TelefonoRestservice} from '../../../servicios/telefono.service';
import {TelefonoInterface} from '../../../interfaces/telefono-interface';

@Component({
  selector: 'app-crear-editar-telefono',
  templateUrl: './crear-editar-telefono.component.html',
  styleUrls: ['./crear-editar-telefono.component.css']
})
export class CrearEditarTelefonoComponent implements OnInit {
  habilitarGuardar = false;
  telefonoAGuardar: TelefonoInterface;
  datosTelefonoEditar = this.data

  constructor(private readonly _dialogRef: MatDialogRef<ClienteComponent>,
              protected _clienteService: ClientesRestService,
              protected _telefonoService: TelefonoRestservice,
              @Inject(MAT_DIALOG_DATA) private  data: { telefono: TelefonoInterface, idCliente: number }
  ) {
  }

  ngOnInit(): void {
  }

  capturarDatosTelefono(evento: TelefonoInterface | boolean) {
    if (typeof evento === 'boolean') {
      this.habilitarGuardar = evento;
    } else {
      this.habilitarGuardar = true;
      this.telefonoAGuardar = evento;
    }
  }

  enviarDatos() {
    if (!this.data.telefono) {
      this._telefonoService.createOne(this.telefonoAGuardar.numero, +this.data.idCliente)
        .subscribe(
          res => {
            this._dialogRef.close(res);
          },
          error => console.error('error creando cliente', error)
        )
    } else {
      if (this.telefonoAGuardar !== undefined) {
        this._telefonoService.updateOne(this.telefonoAGuardar, +this.data.telefono.id)
          .subscribe(
            res => {
              const telefonoEditado = this.telefonoAGuardar
              telefonoEditado.id = this.data.telefono.id;
              this._dialogRef.close(telefonoEditado)
            },
            error => {
              console.error('Error actualizado telefono...', error)
            }
          )
      }
    }

  }

}
