import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {debounceTime} from 'rxjs/operators';
import {TelefonoInterface} from '../../../interfaces/telefono-interface';

@Component({
  selector: 'app-formulario-telefono',
  templateUrl: './formulario-telefono.component.html',
  styleUrls: ['./formulario-telefono.component.css']
})
export class FormularioTelefonoComponent implements OnInit {
  @Input() datosTelefono: { telefono: TelefonoInterface, idCliente: number };
  @Output() Telefono: EventEmitter<Object | boolean> = new EventEmitter<Object | boolean>();
  formularioTelefono: FormGroup;

  constructor() {
    this.formularioTelefono = new FormGroup(
      {
        numero: new FormControl('',
          [
            Validators.maxLength(12),
            Validators.minLength(7),
            Validators.pattern('[0-9]*'),
          ]
        )
      }
    )
  }

  ngOnInit(): void {
    if (this.datosTelefono.telefono) {
      this.formularioTelefono.patchValue(this.datosTelefono.telefono);
    }
    this.escucharFormulario();
  }

  escucharFormulario() {
    this.formularioTelefono
      .valueChanges
      .pipe(
        debounceTime(200)
      )
      .subscribe(
        valoresFormulario => {
          const esFormularioValido = this.formularioTelefono.valid;
          if (!esFormularioValido) {
            this.Telefono.emit(false);
          } else {
            this.Telefono.emit(valoresFormulario);
          }

        }
      )
  }

}
