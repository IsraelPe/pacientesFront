import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormularioTelefonoComponent} from './formulario-telefono.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';


@NgModule({
  declarations: [FormularioTelefonoComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule
  ],
  exports: [
    FormularioTelefonoComponent
  ]
})
export class FormularioTelefonoModule {
}
