import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormularioTelefonoComponent } from './formulario-telefono.component';

describe('FormularioTelefonoComponent', () => {
  let component: FormularioTelefonoComponent;
  let fixture: ComponentFixture<FormularioTelefonoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormularioTelefonoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormularioTelefonoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
