import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {TelefonoRestservice} from '../servicios/telefono.service';
import {LazyLoadEvent, MenuItem} from 'primeng';
import {TelefonoInterface} from '../interfaces/telefono-interface';
import {ClienteInterface} from '../interfaces/cliente-interface';
import {MatDialog} from '@angular/material/dialog';
import {CrearEditarTelefonoComponent} from './modales/crear-editar-telefono/crear-editar-telefono.component';
import {ClientesRestService} from '../servicios/cliente.service';

@Component({
  selector: 'app-telefono',
  templateUrl: './telefono.component.html',
  styleUrls: ['./telefono.component.css']
})
export class TelefonoComponent implements OnInit {
  idCliente: number;
  filasTablaTelefonos = 5;
  datosTelefonos: TelefonoInterface[];
  cantidadTelefonos: number;
  nombreCliente: string;
  columnasTelefono = [
    {
      header: "Número",
      campo: "numero"
    },
    {
      header: "Opciones"
    }
  ]
  migasDePan: MenuItem[];

  constructor(
    private readonly _activatedRoute: ActivatedRoute,
    private readonly _telefonService: TelefonoRestservice,
    private readonly _clienteService: ClientesRestService,
    public dialog: MatDialog,
  ) {
  }

  ngOnInit(): void {
    this.migasDePan = [
      {label: 'Menú principal', routerLink: '/'},
      {label: 'Pacientes', routerLink: '/menu/cliente/gestion-cliente'},
      {label: 'Teléfonos'}
    ]
    this._activatedRoute.params.subscribe(
      (parametros: { idCliente: number }) => {
        this.idCliente = parametros.idCliente;
        this.cargarNombreCliente();
        this.cargarDatosLazy();
      }
    )

  }

  cargarDatosLazy(event?: LazyLoadEvent) {
    if (event === undefined) {
      this._telefonService.findAllPorId(0, this.filasTablaTelefonos, this.idCliente)
        .subscribe(
          telefonos => {
            this.datosTelefonos = telefonos[0];
            this.cantidadTelefonos = telefonos[1];
          },
          error => console.error('Error trayendo datos...', error)
        )
    } else {
      this._telefonService.findAllPorId(event.first, (event.first + event.rows), this.idCliente)
        .subscribe(
          telefonos => {
            this.datosTelefonos = telefonos[0];
            this.cantidadTelefonos = telefonos[1];
          },
          error => console.error('Error trayendo datos...', error)
        )
    }
  }

  cargarNombreCliente() {
    this._clienteService.findOne(this.idCliente).subscribe(
      (res: ClienteInterface) => this.nombreCliente = res.apellidos + ' ' + res.nombres
    )
  }

  eliminarTelefono(fila: TelefonoInterface) {
    return this._telefonService.remove(fila.id).subscribe(
      res => {
        this.cargarDatosLazy()
      }
    )
      ;
  }

  abrirModalCrearEditarTelefono(telefono?: ClienteInterface) {
    const dialogRef = this.dialog.open(CrearEditarTelefonoComponent, {
      height: '300px',
      width: '600px',
      data: {telefono, idCliente: this.idCliente},
    });

    const resultadoModal$ = dialogRef.afterClosed();
    resultadoModal$.subscribe((registroRes) => {
        if (telefono && registroRes) {
          const indiceCliente = this.datosTelefonos.findIndex(datos => telefono.id === datos.id);
          this.datosTelefonos[indiceCliente] = registroRes;
        } else {
          this.cargarDatosLazy();
        }
      },
      error => {
        console.error('Error...')
      }
    );
  }

}
