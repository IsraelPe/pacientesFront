import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {RouterModule} from '@angular/router';
import {HttpClientModule} from '@angular/common/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ClientesRestService} from './modulos/servicios/cliente.service';
import {TelefonoRestservice} from './modulos/servicios/telefono.service';
import {TipoExamenRestService} from './modulos/servicios/tipo-examen.service';
import {ExamenRestService} from './modulos/servicios/examen.service';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule,
    HttpClientModule,
    BrowserAnimationsModule
  ],
  providers: [
    ClientesRestService,
    TelefonoRestservice,
    TipoExamenRestService,
    ExamenRestService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
