import {Component, EventEmitter, Input, Output} from '@angular/core';
import {debounceTime} from 'rxjs/operators';
import {FormControl, FormGroup} from '@angular/forms';

@Component({
  selector: 'app-form-busqueda',
  templateUrl: './form-busqueda.component.html',
  styleUrls: ['./form-busqueda.component.css']
})
export class FormBusquedaComponent {
  @Output() Busqueda: EventEmitter<Object | boolean> = new EventEmitter<Object | boolean>();
  @Input() ayuda: string;
  @Input() busquePor: string;
  formBusqueda: FormGroup;

  constructor() {

    this.formBusqueda = new FormGroup({
      busqueda: new FormControl('',),

    });
    this.escucharFormulario();
  }

  escucharFormulario() {
    this.formBusqueda
      .valueChanges
      .pipe(
        debounceTime(200)
      )
      .subscribe(
        valoresFormulario => {
          const esFormularioValido = this.formBusqueda.valid;
          if (!esFormularioValido) {

            this.Busqueda.emit(false);

          } else {
            this.Busqueda.emit(valoresFormulario);
          }

        }
      );
  }


}
