import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormBusquedaComponent} from './form-busqueda.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';


@NgModule({
  declarations: [
    FormBusquedaComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule
  ],
  exports: [
    FormBusquedaComponent
  ]
})
export class FormBusquedaModule {
}
