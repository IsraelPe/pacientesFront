import {Component, EventEmitter, Output} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {debounceTime} from 'rxjs/operators';

@Component({
  selector: 'app-filtro-fecha',
  templateUrl: './filtro-fecha.component.html',
  styleUrls: ['./filtro-fecha.component.css']
})
export class FiltroFechaComponent {

  @Output() Busqueda: EventEmitter<Object | boolean> = new EventEmitter<Object | boolean>();
  formFiltroFecha: FormGroup;

  constructor() {

    this.formFiltroFecha = new FormGroup({
      busqueda: new FormControl('',),

    });
    this.escucharFormulario();
  }

  escucharFormulario() {
    this.formFiltroFecha
      .valueChanges
      .pipe(
        debounceTime(200)
      )
      .subscribe(
        valoresFormulario => {
          const esFormularioValido = this.formFiltroFecha.valid;
            if (!esFormularioValido) {

            this.Busqueda.emit(false);

          } else {
            this.Busqueda.emit(valoresFormulario);
          }

        }
      );
  }

  limpiarFecha() {
    this.formFiltroFecha.setValue({busqueda: ''})
  }
}
