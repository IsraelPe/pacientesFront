import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FiltroFechaComponent } from './filtro-fecha.component';
import {ReactiveFormsModule} from '@angular/forms';



@NgModule({
  declarations: [FiltroFechaComponent],
  exports: [
    FiltroFechaComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule
  ]
})
export class FiltroFechaModule { }
