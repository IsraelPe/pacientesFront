import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

const routes: Routes = [
  {
    path: 'menu',
    loadChildren: () => import('./modulos/menu/menu.module').then(modulo => modulo.MenuModule)
  },
  {
    path: '',
    pathMatch: 'full',
    redirectTo: '/menu/menu-principal'
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
